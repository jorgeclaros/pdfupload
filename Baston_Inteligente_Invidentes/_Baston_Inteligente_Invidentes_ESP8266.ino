/*
  Este proyecto fue obtenido de la Comunidad Maker, asi mismo ya es parte de esta. 
  El proyecto esta basado en el video https://www.youtube.com/watch?v=9HGswMN9U2I,
  pero plantea los siguientes cambios:
  -Uso de ESP8266 para abaratar costes
  -Uso del modulo Wifi para que el asistete del no vidente busque el baston por 
   medio del Buzzer.
  -uso de un Buzzer para funcionar junto al vibrador, tambien sirve para el sonido
   de inicio que alerta el estado de bateria, finalmente Emite un sonido cuando se
   lo busca por Wifi.
   Mejoras realizadas: Jorge Claros
*/


#include <ESP8266WiFi.h>
const int buzzer=13;
const int trigPin = 12;
const int echoPin = 14;
const int vibrador=15;
/*
ECHO a D5
TRIG a D6
BUZZER a D7
VIBRADOR a D8
*/
long duration;
float distanceCm;
const char* ssid = "NombreRedWifi"; 
const char* password = "ClaveRedWifi";
int encendido=0;
// Direccion IP
IPAddress local_IP(192, 168, 1, 222);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);
IPAddress secondaryDNS(8, 8, 4, 4);
WiFiServer server(80);

void setup() {
  Serial.begin(9600);   
  pinMode(buzzer, OUTPUT);
  pinMode(vibrador, OUTPUT);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  digitalWrite(buzzer, LOW);
  // Connect to WiFi network
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
  Serial.println("Error en la Configuracion de IP");
  }

  if(WiFi.begin(ssid, password)){
	  if(WiFi.status() == WL_CONNECTED) {
		server.begin();  
		} else {
		Serial.println("No se puede Iniciar Servidor");
		}	  
  } else {
		Serial.println("No se puede conectar a la Red Wifi");	  
  }
}

void loop() {
  // Encendido
  /* Sonido de Encendido 
  Al Prender el Baston suena 3 veces, si el sonido es de bajo volumen
  indica que la bateria del baston se esta agotando.
  */
  if(encendido==0){
    for(int i=1;i<=3;i++){
      digitalWrite(buzzer,HIGH);
      delay(500);
      digitalWrite(buzzer,LOW);
      delay(500);
    }
    encendido=1;
    } else {
      digitalWrite(buzzer,LOW);
    }
    // Ultrasonico
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distanceCm = duration * 0.034/2;
  Serial.println(distanceCm);
  if(distanceCm<=20){
    digitalWrite(vibrador,HIGH);  
    digitalWrite(buzzer,HIGH);
    } else {
    digitalWrite(vibrador,LOW);
    digitalWrite(buzzer,LOW);
    }
    delay(1000);
    
// Conexion WIFI
if(WiFi.status() == WL_CONNECTED) {
        WiFiClient client = server.available();
        if (!client) {
          return;
        }
        while (! client.available())
        {
          delay (1);
        }
        String req = client.readStringUntil('\r');
        client.flush();
       
      // Match the request      
  	if(req.indexOf("/find") != -1)
        {
          for(int i=1;i<=5;i++){
            digitalWrite(buzzer,HIGH);
            delay(500);
            digitalWrite(buzzer,LOW);
            delay(500);
          }
        }
      // Return the response
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println("Connection: close");
      client.println("");  
      client.println("<!DOCTYPE HTML>");
      client.println("<HTML>");
      client.println("<BODY ALIGN='CENTER' BGCOLOR='#D6EAF8'>");
      client.println("<H1 style='font-size:60px; color:#17202A'>Baston para Invidentes</H1>");
      client.println("<BR />");
      client.println("<A href=\"/find\"\"> <button style='background-color:#92969A;border: none;color: white;padding: 20px;text-align: center;text-decoration: none;display: inline-block;font-size: 190px;margin: 4px 2px;cursor: pointer;border-radius: 50%;'>&#128266;</button> </A>");
      client.println("</BODY>");
      client.println("</HTML>");
      delay(1);
      }
}
