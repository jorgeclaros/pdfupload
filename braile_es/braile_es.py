/*
  La idea de este proyecto fue obtenido de la Comunidad Maker, asi mismo ya es parte de esta. 
  El proyecto esta basado en el video https://www.youtube.com/watch?v=05jDQV4_uXU&t=190s,
  pero plantea los siguientes cambios:
  -Se usa para el sistema Braile, no para controlar multimedia como muestra el video de youtube.
  -Arduino para el control del hardware y python para la PC.
  -Incluye minisculas, mayusculas, numeros, letras con acentos y simbolos gramaticos basicos, usados en 
   el sistema braile en Español.
   Author: Jorge Claros
*/

import serial #Serial imported for Serial communication
import time #Required to use delay functions
import pyautogui

ArduinoSerial = serial.Serial('com4',9600) #Create Serial port object called arduinoSerialData
time.sleep(2) #wait for 2 seconds for the communication to get established

while 1:
    incoming = str (ArduinoSerial.readline()) #read the serial data and print it as line
    print incoming
    
    if len(incoming)==1:
        pyautogui.press(incoming)  
    else:
        if 'ene' in incoming:
            pyautogui.hotkey('ñ')  
        if 'upuntos' in incoming:
            pyautogui.hotkey('shift', '´', 'u')  
        if 'aace' in incoming:
            pyautogui.hotkey('´', 'a')  
        if 'eace' in incoming:
            pyautogui.hotkey('´', 'e')  
        if 'iace' in incoming:
            pyautogui.hotkey('´', 'i')  
        if 'oace' in incoming:
            pyautogui.hotkey('´', 'o')  
        if 'uace' in incoming:
            pyautogui.hotkey('´', 'u')

    incoming = "";
    
