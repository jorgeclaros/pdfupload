/*
  La idea de este proyecto fue obtenido de la Comunidad Maker, asi mismo ya es parte de esta. 
  El proyecto esta basado en el video https://www.youtube.com/watch?v=05jDQV4_uXU&t=190s,
  pero plantea los siguientes cambios:
  -Se usa para el sistema Braile, no para controlar multimedia como muestra el video de youtube.
  -Arduino para el control del hardware y python para la PC.
  -Incluye minisculas, mayusculas, numeros, letras con acentos y simbolos gramaticos basicos, usados en 
   el sistema braile en Español.
   Author: Jorge Claros
*/

// Botones
const int btn1 = 2;
const int btn2 = 3;
const int btn3 = 4;
const int btn4 = 5;
const int btn5 = 6;
const int btn6 = 7;
const int time=300;
boolean num = false;
boolean may = false;
boolean coma = false;
boolean adm = false;
boolean inte = false;
void setup() {
  Serial.begin(9600); 
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(btn3, INPUT);
  pinMode(btn4, INPUT);
  pinMode(btn5, INPUT);
  pinMode(btn6, INPUT);
}

void loop() {
int btnp1= digitalRead(btn1);
int btnp2= digitalRead(btn2);
int btnp3= digitalRead(btn3);
int btnp4= digitalRead(btn4);
int btnp5= digitalRead(btn5);
int btnp6= digitalRead(btn6);

//ABECEDARIO
if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== HIGH) {
  Serial.println("numero");
  num=!num;
  }
  else
if (btnp1== LOW && btnp2== LOW && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== HIGH) {
  Serial.println("mayu");
	may=!may;
  }
  else
if(num==true){
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
    Serial.println("1");
    }
    else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
    Serial.println("2");
    }
    else
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
    Serial.println("3");
    }
    else
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
    Serial.println("4");
    }
    else
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
    Serial.println("5");
    }
    else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
    Serial.println("6");
    }
    else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
    Serial.println("7");
    }
    else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
    Serial.println("8");
    }
    else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
    Serial.println("9");
    }
    else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
    Serial.println("0");
    }
} else {		
	//ABECEDARIO
	if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
	Serial.println("a");
    }
    else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
    Serial.println("b");
    }
    else
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
    Serial.println("c");
    }           
    else
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
		Serial.println("d");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== LOW && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
		Serial.println("e");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
		Serial.println("f");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
		Serial.println("g");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
		Serial.println("h");
	  }
	  else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
		Serial.println("i");
	  }
	  else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
		Serial.println("j");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
		Serial.println("k");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
		Serial.println("l");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
		Serial.println("m");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
		Serial.println("n");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
		Serial.println("o");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
		Serial.println("p");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
		Serial.println("q");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
		Serial.println("r");
	  }
	  else
    if (btnp1== LOW && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== LOW) {
		Serial.println("s");
	  }
	  else
    if (btnp1== LOW && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
		Serial.println("t");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== HIGH) {
		Serial.println("u");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== HIGH) {
		Serial.println("v");
	  }
	  else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== HIGH) {
		Serial.println("w");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== HIGH) {
		Serial.println("x");
	  }
	  else		  
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== HIGH) {
		Serial.println("y");
	  }
	  else
    if (btnp1== HIGH && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== HIGH && btnp6== HIGH) {
		Serial.println("z");
	  }
	  //LETRAS ESPAÑOL
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== HIGH && btnp5== HIGH && btnp6== HIGH) {
		if(may==true){
		Serial.println("enem");			
		} else {
		Serial.println("ene");
		}
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== HIGH) {
		Serial.println("simy");
	  }
	  else
    if (btnp1== HIGH && btnp2== HIGH && btnp3== HIGH && btnp4== LOW && btnp5== HIGH && btnp6== HIGH) {
		if(may==true){
		Serial.println("aacem");
		} else {
		Serial.println("aace");
		}
	  }
	  else
    if (btnp1== LOW && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== HIGH) {
		if(may==true){
		Serial.println("eacem");
		} else {
		Serial.println("eace");
		}
	  }
	  else
    if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== HIGH  && btnp5== LOW && btnp6== LOW) {
		if(may==true){
		Serial.println("iacem");
		} else {
		Serial.println("iace");
		}
	  }
	  else
    if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== LOW && btnp6== HIGH) {
		if(may==true){
		Serial.println("oacem");
		} else {
		Serial.println("oace");
		}
	}
	else
    if (btnp1== LOW && btnp2== HIGH && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== HIGH) {
		if(may==true){
		Serial.println("uacem");
		} else {
		Serial.println("uace");
		}
	}
	else
	//ARITMETICA
  if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== HIGH) {
	Serial.println("gui");
	}
	else
	// SIGNOS SIMPLES
  if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
	Serial.println("pun");
	}
	else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
		Serial.println("coma");
	}
	else
    if (btnp1== LOW && btnp2== HIGH && btnp3== HIGH && btnp4== LOW && btnp5== LOW && btnp6== LOW) {
		Serial.println("pcoma");
	}
	else
    if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
		Serial.println("2pun");
	}
	else
  if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
	Serial.println("ast");
	}
	else
  if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== HIGH) {
	Serial.println("para");
	}
	else
  if (btnp1== LOW && btnp2== LOW && btnp3== HIGH && btnp4== HIGH && btnp5== HIGH && btnp6== LOW) {
	Serial.println("parc");
	}
	// SIGNOS DOBLES: APERTURA y CIERRE
	if (btnp1== HIGH && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== HIGH) {
		if(coma==false){
			Serial.println("comaa");
		} else {
			Serial.println("comac");
		}
		coma=!coma;
	}
	else
	if (btnp1== LOW && btnp2== HIGH && btnp3== HIGH && btnp4== LOW && btnp5== HIGH && btnp6== LOW) {
		if(adm==false){
			Serial.println("adma");
		} else {
			Serial.println("admc");
		}
		adm=!adm;
	}
	else
	if (btnp1== LOW && btnp2== HIGH && btnp3== LOW && btnp4== LOW && btnp5== LOW && btnp6== HIGH) {
		if(inte==false){
			Serial.println("intea");
		} else {
			Serial.println("intec");
		}
		inte=!inte;
	}
	}
  delay(time); 
}
