/*
  La idea de este proyecto fue obtenido de la Comunidad Maker, asi mismo ya es parte de esta. 
  El proyecto esta hecho con documentacion oficial de arduino. 
   Author: Jorge Claros
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
const int led_rojo = 2;
const int led_verde = 3;
const int bocina = 4;
long valors;
long valorp;
long gas;
LiquidCrystal_I2C lcd(0x27, 20, 4);

void setup()
{
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(bocina, OUTPUT);
  pinMode(led_rojo, OUTPUT);
  pinMode(led_verde, OUTPUT);
  digitalWrite(led_rojo, LOW);
  digitalWrite(led_verde, LOW);
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Detector de Gas");
  lcd.setCursor(0, 1);
  lcd.print("Domiciliario");
  delay(2000);
  lcd.clear();
}
void loop()
{
  valors = analogRead(A0);
  valorp = analogRead(A1);
  gas=valors-valorp;
  lcd.setCursor(0, 0);
  lcd.print("AMB:");
  lcd.print(valors);
  lcd.print(" NIV:");
  lcd.println(gas);
  
  if (gas >= 20) {
    lcd.setCursor(0, 1);
    lcd.print(".....Peligro.....");
    digitalWrite(bocina, HIGH);
    digitalWrite(led_verde, HIGH);
    digitalWrite(led_rojo, LOW);
    delay(1000);
  } else {
    lcd.setCursor(0, 1);
    lcd.print(".....Normal.....");
    digitalWrite(bocina, LOW);
    digitalWrite(led_verde, LOW);
    digitalWrite(led_rojo, HIGH);
    delay(1000);
  }
}
