/*
  La idea de este proyecto fue obtenido de la Comunidad Maker, asi mismo ya es parte de esta. 
  El proyecto esta hecho con documentacion oficial de arduino. 
   Author: Jorge Claros
*/

const int m1a = 3;            //Motor 1, pin 10 del arduino va al pin 15 del L293B.
const int m1b = 2;             //Motor 1, pin 9 del arduino va al pin 10 del L293B.
const int m2a = 6;            //Motor 2, pin 12 DEL arduino va al pin 2 del L293B.
const int m2b = 5;            //Motor 2, pin 11 del arduino va al pin 7 del L293B.
const int buzzer=4;
boolean limpio=false;
int cont=0;
const int trigPin = 7;
const int echoPin = 8;
long duration;
float distanceCm;
float distanceCmTemp;
unsigned long tiempo = 0;

void setup() 
{  
pinMode(m1a, OUTPUT);  // Digital pin 10 set as output Pin
pinMode(m1b, OUTPUT);  // Digital pin 9 set as output Pin
pinMode(m2a, OUTPUT);  // Digital pin 12 set as output Pin
pinMode(m2b, OUTPUT);  // Digital pin 11 set as output Pin
pinMode(buzzer, OUTPUT);  // Digital pin 11 set as output Pin
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT);
sonidoPrendido();
Serial.begin(9600);
}

void loop()
{ 
    tiempo=millis();
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    distanceCm = duration * 0.034/2;
    if(limpio==false){
      if(distanceCm<=10){
        if((tiempo%5000)==0){
          derecha();
          delay(500);
        }        
        if(cont<=25){            
              derecha();
              delay(1000);              
              cont++;
             } else {
              parar();
              sonido();
              limpio=true;
            }
     } else {
          adelante();
        }
    } else {
        parar();
    }
}

void adelante(){
      digitalWrite(m1a, 0);
      digitalWrite(m1b, 63);
      digitalWrite(m2a, 0);
      digitalWrite(m2b, 63); 
}

void atras(){
      digitalWrite(m1a, 63);
      digitalWrite(m1b, 0);
      digitalWrite(m2a, 63);
      digitalWrite(m2b, 0);
}

void derecha(){
    digitalWrite(m1a, 0);
    digitalWrite(m1b, 0);
    digitalWrite(m2a, 63);
    digitalWrite(m2b, 0);
}

void izquierda(){
    digitalWrite(m1a, 63);
    digitalWrite(m1b, 0);
    digitalWrite(m2a, 0);
    digitalWrite(m2b, 0); 
}

void parar(){
    digitalWrite(m1a, 0);
    digitalWrite(m1b, 0);
    digitalWrite(m2a, 0);
    digitalWrite(m2b, 0); 
}

void sonido(){
    digitalWrite(buzzer, HIGH);
    delay(3000);
    digitalWrite(buzzer, LOW);
}

void sonidoPrendido(){
    tone(buzzer, 220, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 220, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 220, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 220, 224.062126563);
    delay(248.957918403);
    delay(1.04166493056);
    tone(buzzer, 261, 224.062126563);
    delay(248.957918403);
}
